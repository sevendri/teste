package snippet;

public class Snippet {
	server:
	  port: 3001
	
	spring:
	  data:
	    cassandra:
	      keyspace-name: btcas002
	      port: 9042
	      contact-points: localhost
	      username: cassandra
	      password: cassandra
}

